package softcare.akash.secureu2;

/**
 * Created by Akash on 12-Nov-16.
 */
   import android.app.Activity;
        import android.os.Bundle;
        import android.webkit.WebView;

public class TrackView extends Activity {

    private WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_view);

        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://smart-city-rak13.c9users.io/");

    }

}
