//package softcare.akash.medicription;
//
///**
// * Created by Akash on 29-Oct-16.
// */
//
//import android.content.Intent;
//
//
//
//
//        import android.content.Intent;
//        import android.graphics.Color;
//        import android.os.Bundle;
//        import android.widget.Toast;
//
//        import com.github.paolorotolo.appintro.AppIntro;
//        import com.github.paolorotolo.appintro.AppIntroFragment;
//
//public class Help2 extends AppIntro {
//
//    String[] arr = {"Welcome! This personal assistant app contains all the emergency features you need to access at the moment of sudden accident. Slide right & explore all the features.",
//            "This will let you know about the nearest OCC (One Stop Crisis Centre) & nearest law assistance centre based on your personal info."
//            , "Enables a lock screen for your device. Keep this app running. Double click volume up & down key for Instant Siren & SMS."
//            , "Allows you to take secret images for proof. Flash & Shutter sound auto turn off. No camera preview. Stores in a new folder."
//            , "Schedule emergency SMS to a number & set a timer when it should be sent to the destination for help."
//            , "Record anything suspicious or use this tool for keeping proof of harassment."
//            , "Allows you to make excuses to get rid of suspicious situation. Also rings your phone even in silent mood."
//            , "Slide right for Navigation Drawer where you can contact the app developers, Human Rights Organization & FAQ section."
//            , "Edit Emergency number & info from Navigation Drawer anytime. Tap the question button anytime for this help menu.\n" +"Thank you & stay safe."
//    };
//    // Please DO NOT override onCreate. Use init.
//    @Override
//    public void init(Bundle savedInstanceState) {
//
//        // Add your slide's fragments here.
//        // AppIntro will automatically generate the dots indicator and buttons.
//        //addSlide(R.layout.intro);
//        //addSlide(second_fragment);
//        //addSlide(third_fragment);
//        //addSlide(fourth_fragment);
//
//        // Instead of fragments, you can also use our default slide
//        // Just set a title, description, background and image. AppIntro will do the rest.
//        addSlide(AppIntroFragment.newInstance("The app", arr[0], /*R.mipmap.ic_launcher Integer.parseInt(null)*/ R.drawable.app_logo ,Color.parseColor( "#B02F1B" ) ));
//        addSlide(AppIntroFragment.newInstance("Info Tab", arr[1], R.drawable.info ,Color.parseColor( "#ca90f2f4")  ));
//        addSlide(AppIntroFragment.newInstance("Lock Screen", arr[2], R.drawable.lock, Color.parseColor("#4C0B5F") ));
//        addSlide(AppIntroFragment.newInstance("Spy Camera", arr[3] , R.drawable.spy, Color.parseColor("#04B4AE")));
//        addSlide(AppIntroFragment.newInstance("Timer SMS", arr[4], R.drawable.timer, Color.parseColor("#75a800")));
//        addSlide(AppIntroFragment.newInstance("Voice Recording",arr[5], R.drawable.record, Color.parseColor("#8000FF")));
//        addSlide(AppIntroFragment.newInstance("Fake Call",arr[6] , R.drawable.fake, Color.parseColor("#405D97")));
//        // addSlide(AppIntroFragment.newInstance("Navigation Drawer", arr[7], R.mipmap.ic_launcher, Color.parseColor("#81725F")));
//        addSlide(AppIntroFragment.newInstance("Communication", arr[8], R.drawable.mail, Color.parseColor("#32196E") ));
//
//        // OPTIONAL METHODS
//        // Override bar/separator color.
//        setBarColor(Color.parseColor("#3F51B5"));
//        setSeparatorColor(Color.parseColor("#2196F3"));
//
//        // Hide Skip/Done button.
//        showSkipButton(true);
//        setProgressButtonEnabled(true);
//
//        // Turn vibration on and set intensity.
//        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest.
//        setVibrate(true);
//        setVibrateIntensity(30);
//
//        //setFadeAnimation();  //not so intrsting
//        setZoomAnimation(); //best
//        //setFlowAnimation();  // 1tar por ekta, not so interesting
//        //setSlideOverAnimation(); //good animation but li'l bit confusing for eyes
//        //setDepthAnimation();  //more like 1tar upor arekta card
//    }
//
//    @Override
//    public void onSkipPressed() {
//        // Do something when users tap on Skip button.
//        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//    }
//
//    @Override
//    public void onDonePressed() {
//        // Do something when users tap on Done button.
//        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//    }
//
//    @Override
//    public void onSlideChanged() {
//        // Do something when the slide changes.
//        //Toast.makeText(getApplicationContext(),"blabla", Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onNextPressed() {
//        // Do something when users tap on Next button.
//    }
//
//}