package softcare.akash.secureu2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.loopj.android.http.*;
import com.nineoldandroids.animation.Animator;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;


/**
 * Created by Akash on 29-Oct-16.
 */

public class mainUI extends Activity {


    public static final int REQUEST_IMAGE_CAPTURE = 1;


    //CardView card, card1,card2,card3,card4,card5,card6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uimain);
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        height = (height* 95)/100;
        width = (width* 90)/100;
//////////////////////////FAB BUTTON CODES///////////////

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

                startActivity(new Intent(getApplicationContext(), Help2.class));

            }
        });
*/
        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card));

//        YoYo.with(Techniques.SlideInLeft)
//                .duration(1100)
//                .playOn(findViewById(R.id.card2));

        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card5));

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card3));

//        YoYo.with(Techniques.SlideInRight)
//                .duration(1100)
//                .playOn(findViewById(R.id.card4));
//
        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card6));

        ImageView img = (ImageView) findViewById(R.id.image6);
//        Blurry.with(getApplicationContext()).capture(findViewById(R.id.image6)).into(img);
        // tiash


        ///////////////////////////left side
        CardView card = (CardView) findViewById(R.id.card);
        card.getLayoutParams().height=(height/3);
        card.getLayoutParams().width=width/2;

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

                startActivity(new Intent(getApplicationContext(), lock.class));

            }
        });


//        CardView card2 = (CardView) findViewById(R.id.card2);
//        card2.getLayoutParams().height=(height/4);
//        card2.getLayoutParams().width=width/2;
//
//        card2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
//                //      .setAction("Action", null).show();
//
//                //startActivity(new Intent(getApplicationContext(), Help.class));
//
//                startActivity(new Intent(getApplicationContext(), patientUI.class));
//
//            }
//        });

        CardView card5 = (CardView) findViewById(R.id.card5);
        card5.getLayoutParams().height=(height/3);
        card5.getLayoutParams().width=width/2;

        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

                startActivity(new Intent(getApplicationContext(), traffic.class));

            }
        });



        //////////////////////////////////////////////right side

        CardView card3 = (CardView) findViewById(R.id.card3);
        card3.getLayoutParams().height=height/3;
        card3.getLayoutParams().width=width/2;

        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

                startActivity(new Intent(getApplicationContext(), TrackView.class));

            }
        });

//        CardView card4 = (CardView) findViewById(R.id.card4);
//        card4.getLayoutParams().height=height/4;
//        card4.getLayoutParams().width=width/2;


        CardView card6 = (CardView) findViewById(R.id.card6);
        card6.getLayoutParams().height=height/3;
        card6.getLayoutParams().width=width/2;

        card6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Welcome to the help menu", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                //startActivity(new Intent(getApplicationContext(), Help.class));

               // startActivity(new Intent(getApplicationContext(), upload2.class));
                dispatchTakePictureIntent();

            }
        });
        ////////////////////////////////////////////////


    }

    //////////////////////////

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
        imagesFolder.mkdirs();

        File image = new File(imagesFolder, "pic.png");
        Uri uriSavedImage = Uri.fromFile(image);

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Toast.makeText(getApplicationContext(), "hoise1", Toast.LENGTH_LONG).show();

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
            Toast.makeText(getApplicationContext(), "hoise", Toast.LENGTH_LONG).show();
            upload();
        }
    }

    void upload() {
        String SERVER_URL = "https://smart-city-rak13.c9users.io/dbquery.php";
        String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
        String imagePath = PATH + "/MyImages/pic.png";

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("query_name", "insertReport");
        params.put("u_id", "2");
        params.put("time", Long.toString(System.currentTimeMillis()));
        params.put("comment", "what is what?");
        try {
            params.put("pic", new File(imagePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        client.post(SERVER_URL, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int i, Header[] headers, String s, Throwable throwable) {
                Log.e("Failed", "Failure");
            }

            @Override
            public void onSuccess(int i, Header[] headers, String s) {
                Log.e("Succ", "Success");
            }
        });

        ///////////////////////////////



//    @Override
//    public void onBackPressed() {
//
//       startActivity(new Intent(getApplicationContext(),MainActivity.class));
//
//
    }
}
