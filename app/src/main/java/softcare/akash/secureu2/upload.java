package softcare.akash.secureu2;

/**
 * Created by Akash on 12-Nov-16.
 */
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;



public class upload extends Activity {

    static int process =0;

    Camera camera;
    Camera.PictureCallback rawCallback;
    Camera.ShutterCallback shutterCallback;
    Camera.PictureCallback jpegCallback;
    private long mLastClickTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera);

//        TextView t1 = (TextView) findViewById(R.id.t1);
//        TranslateAnimation animate ;
//        //t1.setVisibility(v.VISIBLE);
//        //t1.setText("working");
//        animate = new TranslateAnimation(-200,0,-200,0);
//        animate.setDuration(1500);
//        animate.setFillAfter(true);
//        t1.startAnimation(animate);
//        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
//        surfaceHolder = surfaceView.getHolder();

        // Installing a SurfaceHolder.Callback to get notified when the
        // underlying surface is created & destroyed
//        surfaceHolder.addCallback(this);
//
//        // deprecated bt needed for on Android versions prior to 3.0
//        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


        Context context = this;
        PackageManager pm = context.getPackageManager();

        // if device dznt support camera
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(getApplicationContext(), "This device has no Camera", Toast.LENGTH_LONG).show();
            return;
        }

        jpegCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera)
            {	//LinearLayout hide = (LinearLayout) findViewById(R.id.capture);
                //hide.setVisibility(View.GONE);
                FileOutputStream outStream = null;
                try
                {
                    File sdCard = Environment.getExternalStorageDirectory();
                    File dir = new File (sdCard.getAbsolutePath() + "/SecureU");
                    dir.mkdirs();
                    File file = new File(dir, "SecureU"+".jpg");
                    outStream = new FileOutputStream(file);

                    //outStream = new FileOutputStream(String.format("/sdcard/habijabi.jpg", System.currentTimeMillis()));
                    outStream.write(data);
                    outStream.close();
                    // Log.d("Log", "onPictureTaken - wrote bytes: " + data.length);
                }
                catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }

                catch (IOException e)
                {
                    e.printStackTrace();
                }

                finally
                {

                }

                Toast.makeText(getApplicationContext(), "Picture Saved in SecureU folder",Toast.LENGTH_LONG).show();
                process = 0;
                //hide.setVisibility(View.VISIBLE);
                refreshCamera();
            }
        };
    }

    public void captureImage(View v) throws IOException
    {
        //take the picture
        //LinearLayout hide = (LinearLayout) findViewById(R.id.capture);
        //hide.setVisibility(View.GONE);
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000 || process == 1)
        {
            Toast.makeText(getApplicationContext(), "Task Processing, PLease Wait",Toast.LENGTH_LONG).show();
            return ;
        }
        process = 1;
        mLastClickTime = SystemClock.elapsedRealtime();
        Camera.Parameters params = camera.getParameters();
        List<Camera.Size> sizes = params.getSupportedPictureSizes();
        // See which sizes the camera supports and choose one of those
        Camera.Size mSize = sizes.get(0);
        params.setPictureSize(mSize.width, mSize.height);
        params.set("orientation", "landscape");
        params.setRotation(90);
        camera.setParameters(params);

        camera.takePicture(null, null, jpegCallback);
    }

    public void refreshCamera() {
//        if (surfaceHolder.getSurface() == null) {
//            // preview surface does not exist
//            return;
//        }

        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        Camera.Parameters param;
        param = camera.getParameters();

        // modify parameter
        //param.setPreviewSize(352, 288);
        //param.setRotation(180);
        camera.setParameters(param);
        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {


//            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {

        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin preview.
        refreshCamera();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            // open the camera
            camera = Camera.open();
        } catch (RuntimeException e) {
            // check for exceptions
            System.err.println(e);
            return;
        }
        Camera.Parameters param;
        param = camera.getParameters();

        // modify parameter
        param.setPreviewSize(352, 288);
        param.setRotation(180);
        camera.setParameters(param);
        try {
            // The Surface has been created, now tell the camera where to draw
            // the preview.
           // camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {
            // check for exceptions
            System.err.println(e);
            return;
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // stop preview and release camera
        camera.stopPreview();
        camera.release();
        camera = null;
    }
   /* @Override
    public void onBackPressed()
    {
        //(Spycamera.this).finish();
        Intent loginscreen = new Intent(this, MainActivity.class);
        //loginscreen.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(loginscreen);
    }*/

}
